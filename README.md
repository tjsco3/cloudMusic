# 基于uni-app云音乐小程序

#### 介绍
基于uni-app云音乐小程序

#### 软件架构
通过Node.js服务获取数据，uni-app实现小程序前端界面；  
基于uni-app前端框架(Vue.js)可实现发布到ios、Android、Web以及微信等各种小程序平台；

#### 安装教程

1.  在NeteaseCloudMusicApi执行`npm install`安装相关依赖
2.  在NeteaseCloudMusicApi执行`node app.js`命令启动服务，监听端口为3000
3.  在HBuilderX编辑器下，运行uni-appCloudMusic项目，选择微信开发者工具运行

#### 首页界面
![首页](uni-appCloudMusic/md-imgs/img_1.png)

#### 音乐播放界面
![音乐播放](uni-appCloudMusic/md-imgs/img_2.png)

#### 视频列表界面
![视频列表](uni-appCloudMusic/md-imgs/img_3.png)

#### 视频播放界面
![视频播放](uni-appCloudMusic/md-imgs/img_4.png)

#### 歌单分类界面
![歌单分类](uni-appCloudMusic/md-imgs/img_5.png)

#### 音乐列表界面
![音乐列表](uni-appCloudMusic/md-imgs/img_6.png)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
