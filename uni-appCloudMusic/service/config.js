// 本地
// export const baseURL = 'http://localhost:3000'
// vercel部署
// export const baseURL = 'https://cloudmusic-tj.vercel.app'
// 线上
export const baseURL = 'https://www.codeman.ink/api'

export const timeout = 500000