(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["pages/profile/profile"],{

/***/ 60:
/*!***********************************************************************************************!*\
  !*** C:/Users/Timothy/Desktop/uni-appCloudMusic/main.js?{"page":"pages%2Fprofile%2Fprofile"} ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(wx, createPage) {

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ 4);
__webpack_require__(/*! uni-pages */ 26);
var _vue = _interopRequireDefault(__webpack_require__(/*! vue */ 25));
var _profile = _interopRequireDefault(__webpack_require__(/*! ./pages/profile/profile.vue */ 61));
// @ts-ignore
wx.__webpack_require_UNI_MP_PLUGIN__ = __webpack_require__;
createPage(_profile.default);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/wx.js */ 1)["default"], __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 2)["createPage"]))

/***/ }),

/***/ 61:
/*!****************************************************************************!*\
  !*** C:/Users/Timothy/Desktop/uni-appCloudMusic/pages/profile/profile.vue ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _profile_vue_vue_type_template_id_60cb334c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./profile.vue?vue&type=template&id=60cb334c& */ 62);
/* harmony import */ var _profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile.vue?vue&type=script&lang=js& */ 64);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _profile_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./profile.vue?vue&type=style&index=0&lang=css& */ 67);
/* harmony import */ var _D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/runtime/componentNormalizer.js */ 35);

var renderjs





/* normalize component */

var component = Object(_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _profile_vue_vue_type_template_id_60cb334c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _profile_vue_vue_type_template_id_60cb334c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null,
  false,
  _profile_vue_vue_type_template_id_60cb334c___WEBPACK_IMPORTED_MODULE_0__["components"],
  renderjs
)

component.options.__file = "pages/profile/profile.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 62:
/*!***********************************************************************************************************!*\
  !*** C:/Users/Timothy/Desktop/uni-appCloudMusic/pages/profile/profile.vue?vue&type=template&id=60cb334c& ***!
  \***********************************************************************************************************/
/*! exports provided: render, staticRenderFns, recyclableRender, components */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_17_0_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_template_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_template_id_60cb334c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--17-0!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/template.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-uni-app-loader/page-meta.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!./profile.vue?vue&type=template&id=60cb334c& */ 63);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_17_0_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_template_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_template_id_60cb334c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_17_0_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_template_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_template_id_60cb334c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "recyclableRender", function() { return _D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_17_0_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_template_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_template_id_60cb334c___WEBPACK_IMPORTED_MODULE_0__["recyclableRender"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "components", function() { return _D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_17_0_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_template_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_template_id_60cb334c___WEBPACK_IMPORTED_MODULE_0__["components"]; });



/***/ }),

/***/ 63:
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--17-0!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/template.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-uni-app-loader/page-meta.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!C:/Users/Timothy/Desktop/uni-appCloudMusic/pages/profile/profile.vue?vue&type=template&id=60cb334c& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns, recyclableRender, components */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "recyclableRender", function() { return recyclableRender; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "components", function() { return components; });
var components
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
}
var recyclableRender = false
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ 64:
/*!*****************************************************************************************************!*\
  !*** C:/Users/Timothy/Desktop/uni-appCloudMusic/pages/profile/profile.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!./node_modules/babel-loader/lib!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--13-1!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/script.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!./profile.vue?vue&type=script&lang=js& */ 65);
/* harmony import */ var _D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 65:
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--13-1!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/script.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!C:/Users/Timothy/Desktop/uni-appCloudMusic/pages/profile/profile.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni) {

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _home = __webpack_require__(/*! ../../service/home */ 30);
var _songs = __webpack_require__(/*! ../../service/songs */ 66);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var app = getApp();
var commonAudio = function commonAudio() {
  Promise.all(/*! require.ensure | components/common-audio */[__webpack_require__.e("common/vendor"), __webpack_require__.e("components/common-audio")]).then((function () {
    return resolve(__webpack_require__(/*! components/common-audio */ 326));
  }).bind(null, __webpack_require__)).catch(__webpack_require__.oe);
};
var _default = {
  components: {
    commonAudio: commonAudio
  },
  data: function data() {
    return {
      h: '',
      showAudio: false,
      scrollStyle: '',
      userInfo: {},
      // 用户信息
      recentPlayList: [],
      // 用户播放记录
      level: {},
      likes: {},
      isActive: false,
      own: [{
        coverImgUrl: 'https://p1.music.126.net/5noG3sFSgZk5vHl9yV_5Mw==/109951165796624203.jpg',
        name: '小众系｜宝岛台湾的温柔海风',
        count: '3首'
      }, {
        coverImgUrl: 'https://p1.music.126.net/khPdkd2uXvB8xbiiYByJVw==/109951165837887565.jpg',
        name: '礼义之邦｜华夏文明五千年，薪火相传永不灭',
        count: '5首'
      }, {
        coverImgUrl: 'https://p1.music.126.net/Vxsf58sMyNjqnWVZa9Wbfw==/18991864347187831.jpg',
        name: '梦幻噪音治愈手册｜自赏派编织的斑斓绮梦',
        count: '2首'
      }, {
        coverImgUrl: 'https://p1.music.126.net/84A7C8abhHyI0wJGN79w0w==/109951164195838881.jpg',
        name: '欧美民谣 I 往昔璀璨如钻 却又锈迹斑斑',
        count: '6首'
      }, {
        coverImgUrl: 'https://p1.music.126.net/RtN6VzXNzOPAOhCiQSfjkQ==/109951163754607895.jpg',
        name: 'Future Beats ◐ 糟糕，是心肌梗塞的感觉！',
        count: '7首'
      }],
      playList: [{
        picUrl: 'https://p2.music.126.net/JOJvZc_7SqQjKf8TktQ_bw==/29686813951246.jpg',
        song: '光辉岁月',
        album: '专辑 - 光辉岁月十五年',
        singer: '歌手 ： Beyond'
      }, {
        picUrl: 'https://p2.music.126.net/8y8KJC1eCSO_vUKf2MyZwA==/109951165796899183.jpg',
        song: '海阔天空',
        album: '专辑 - 海阔天空',
        singer: '歌手 ： Beyond'
      }, {
        picUrl: 'https://p2.music.126.net/zZtUDuWk6qIe3ezMt4UMjg==/109951165796417308.jpg',
        song: '真的爱你',
        album: '专辑 - Beyond 25th Anniversary',
        singer: '歌手 ： Beyond'
      }, {
        picUrl: 'https://p2.music.126.net/HDdQFXVIlRvR96s0mQyu9g==/109951164170257125.jpg',
        song: '挪威的森林',
        album: '专辑 - 爱情的尽头',
        singer: '歌手 ： 伍佰 & China Blue'
      }, {
        picUrl: 'https://p2.music.126.net/zDdl2mtUVUpQwBo4Cl7-1w==/109951164811431723.jpg',
        song: '突然的自我',
        album: '专辑 - 忘情1015精选辑',
        singer: '歌手 ： 伍佰 & China Blue'
      }, {
        picUrl: 'https://p2.music.126.net/L570AKxOSH6U4vEIMoHGXQ==/45079976751506.jpg',
        song: '梦醒时分',
        album: '专辑 - 单程车票',
        singer: '歌手 ： 伍佰 & China Blue'
      }, {
        picUrl: 'https://p2.music.126.net/IYmWoRF9sLry9dkzYeFXPQ==/54975581405042.jpg',
        song: '无地自容',
        album: '专辑 - 黑豹1',
        singer: '歌手 ： 黑豹乐队'
      }, {
        picUrl: 'https://p2.music.126.net/IYmWoRF9sLry9dkzYeFXPQ==/54975581405042.jpg',
        song: 'Dont Break My Heart',
        album: '专辑 - 黑豹1',
        singer: '歌手 ： 黑豹乐队'
      }, {
        picUrl: 'https://p2.music.126.net/e_2VbSuHFxTpk1Wl1oa6-w==/109951163753604263.jpg',
        song: '月亮与灵魂',
        album: '专辑 - 一部分',
        singer: '歌手 ： 张楚'
      }, {
        picUrl: 'https://p1.music.126.net/C6txAWMGlVmAcAD37RVutQ==/122045790684028.jpg',
        song: '蓝莲花',
        album: '专辑 - 时光.漫步',
        singer: '歌手 ： 许巍'
      }]
    };
  },
  onLoad: function onLoad() {
    // let userInfo = wx.getStorageSync('userInfo')
    // if(userInfo) {
    // 	this.userInfo = JSON.parse(userInfo)
    // 	this._getRecentPlayList(1)
    // 	this._getLevel()
    // 	this._userPlayList(this.userInfo.userId)
    // }
    console.log('profile');
    uni.$on('login', function (data) {
      console.log(data);
      this.userInfo = data.userInfo;
    });
    this.h = uni.getSystemInfoSync().statusBarHeight;
  },
  onUnload: function onUnload() {
    // 移除监听事件  
    uni.$off('login');
  },
  methods: {
    _userPlayList: function _userPlayList(id) {
      var _this = this;
      (0, _songs.userPlayList)(id).then(function (res) {
        var own = res.playlist;
        _this.likes = res.playlist[0];
        _this.own = own.slice(1);
      });
    },
    _getLevel: function _getLevel() {
      var _this2 = this;
      (0, _home.getLevel)().then(function (res) {
        _this2.level = res.data;
      });
    },
    _getRecentPlayList: function _getRecentPlayList(type) {
      var _this3 = this;
      var params = {
        uid: this.userInfo.userId,
        type: type
      };
      (0, _home.getRecentPlayList)(params).then(function (res) {
        _this3.recentPlayList = res.weekData;
      });
    },
    chooseSong: function chooseSong(e) {
      var id = e.currentTarget.id;
      app.addSong(id);
      (0, _songs.getSongDetail)(id).then(function (res) {
        app.playList(res.songs);
      });
      uni.navigateTo({
        url: '/pages/song-play/song-play?id=' + id
      });
    },
    toLogin: function toLogin() {
      uni.reLaunch({
        url: '/pages/login/login'
      });
    },
    chooselist: function chooselist(e) {
      var id = e.currentTarget.id;
      uni.navigateTo({
        url: '/pages/song-list/song-list?id=' + id
      });
    },
    activePlay: function activePlay() {
      this.isActive = this.isActive == false ? true : false;
      // let pid = this.likes.id
      // getSonglist(pid).then(res => {
      // 	let index = Math.floor(Math.random() * 172)
      // 	let id = res.playlist.tracks[index].id
      // 	activeList(pid, id).then(res => {
      // 		app.globalData.index = -1
      // 		app.addSong(id)
      // 		app.playList(res.data)
      // 		uni.navigateTo({
      // 			url: '/pages/song-play/song-play?active=1&pid=' + pid,
      // 		})
      // 	})
      // })
    }
  },
  onShow: function onShow() {
    var h = this.h;
    if (app.globalData.id && app.globalData.playList.length > 0) {
      this.scrollStyle = "height: calc(100% - ".concat(h + 44 + 60, "px);top:").concat(h + 44, "px;");
      this.showAudio = true;
    } else {
      this.scrollStyle = "height: calc(100% - ".concat(h + 44, "px);top:").concat(h + 44, "px;");
    }
  }
};
exports.default = _default;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 2)["default"]))

/***/ }),

/***/ 67:
/*!*************************************************************************************************************!*\
  !*** C:/Users/Timothy/Desktop/uni-appCloudMusic/pages/profile/profile.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!./node_modules/mini-css-extract-plugin/dist/loader.js??ref--6-oneOf-1-0!./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-1-1!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--6-oneOf-1-2!./node_modules/postcss-loader/src??ref--6-oneOf-1-3!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!./profile.vue?vue&type=style&index=0&lang=css& */ 68);
/* harmony import */ var _D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_x_HbuilderX_HBuilderX_3_7_3_20230223_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_profile_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 68:
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js??ref--6-oneOf-1-0!./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-1-1!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--6-oneOf-1-2!./node_modules/postcss-loader/src??ref--6-oneOf-1-3!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!C:/Users/Timothy/Desktop/uni-appCloudMusic/pages/profile/profile.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin
    if(false) { var cssReload; }
  

/***/ })

},[[60,"common/runtime","common/vendor"]]]);
//# sourceMappingURL=../../../.sourcemap/mp-weixin/pages/profile/profile.js.map